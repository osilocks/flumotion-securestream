from flumotion.component.plugs.requestmodifier import RequestModifierPlug

class RequestModifierSecureUrlPlug(RequestModifierPlug):

    logCategory = 'SecureUrl'

    def start(self, component):
        properties = self.args['properties']
        self._secret_key = properties['SECRET-KEY']
        self.log("Secret Key: %s", self._secret_key)

    def modify(self, request):
		"Decrypt the request url using the secret key"
		from pprint import pprint
		self.log("Encrypted Request Uri: %s" % request.uri)
#        if self._argument in request.args:
#            if self._triggers & python.set(request.args[self._argument]):
#                filename = os.path.basename(urllib.unquote_plus(request.path))
#                filename = filename.encode('UTF-8')
#                filename = urllib.quote(filename)
#                header = "attachment; filename*=utf-8''%s" % filename
#                request.setHeader('Content-Disposition', header)

