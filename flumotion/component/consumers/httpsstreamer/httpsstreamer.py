from flumotion.component.common.streamer.multifdsinkstreamer import \
        MultifdSinkStreamer

__all__ = ['HTTPSStreamer']
__version__ = "$Rev$"


class HTTPSStreamer(MultifdSinkStreamer):
    "A streamer which utilizes some secure principles"
	pass
